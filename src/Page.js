import React from "react";
import Image from "./Modules/Views/Image";
import Description from "./Modules/Views/Description";
import List from "./Modules/Views/List";
import OrderList from "./Modules/Views/OrderList";
import Images from "./Modules/Views/Images";
import Header from "./Modules/Header";
import Footer from "./Modules/Footer";
import ArticleHeader from "./Modules/ArticleHeader";

function ComponentSelector(section, index) {
  switch (section.type) {
    case "Image":
      return <Image key={index} data={section} />;
    case "Images":
      return <Images key={index} data={section} />;
    case "Description":
      return <Description key={index} data={section} />;
    case "List":
      return <List key={index} data={section} />;
    case "OrderList":
      return <OrderList key={index} data={section} />;
    default:
      return "";
  }
}

function Sections(props) {
  return (
    <section>
      {props.data.map((section, index) => {
        return ComponentSelector(section, index);
      })}
    </section>
  );
}

const Page = props => {
  if (!props.page) {
    return <div>Sorry, but page was not found</div>;
  }

  return (
    <React.Fragment>
        <Header />
        <main>
          <article>
            <ArticleHeader data={props.page.header}/>
            <Sections data={props.page.sections} />
          </article>
        </main>
        <Footer/>
    </React.Fragment>
  );
};

export default Page;
