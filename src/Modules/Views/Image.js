import React from "react";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  root: {
    maxWidth: "700px",
    height:"auto",
    margin:"auto"
  },
  image:{
    width:"100%",
    margin:"auto"
  }
}));

export default function Image(props) {
  const classes = useStyles();

  return (
    <section className={classes.root}>
      <div>
        <figure>
          <img className={classes.image} src={props.src} alt={props.alt} />
          <figcaption>{props.heading}</figcaption>
        </figure>
      </div>
    </section>
  );
}