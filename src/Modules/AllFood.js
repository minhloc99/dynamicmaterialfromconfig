import React from "react";
import { Link } from "react-router-dom";

function AllFood(props){
  const allFood=[
      {id:"tohe", title:"Tò he"}
  ];

  return (
    <div>
    <ul>
      {allFood.map(f => (
        <li key={f.id}>
          <Link to={`/food/${f.id}`}>{f.title}</Link>
        </li>
      ))}
    </ul>
  </div>
  )
};

export default AllFood;
