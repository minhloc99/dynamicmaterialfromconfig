import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import AllFood from "./AllFood";
import Tohe from "../Pages/Food/Tohe";
// import Miquang from "../components/Pages/Food/Miquang";
// import Pho from "../components/Pages/Food/Pho";

const FoodRouter = () => (
  <BrowserRouter>
    <Switch>
    <Route exact path="/food" component={AllFood} />
    <Route path="/food/tohe" component={Tohe} />
    
    {/* <Route path="/food/pho" component={Pho} />
    <Route path="/food/miquang" component={Miquang} /> */}
  </Switch></BrowserRouter>

);

export default FoodRouter;
