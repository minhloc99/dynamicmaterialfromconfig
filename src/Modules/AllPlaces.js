import React from "react";
import { Link } from "react-router-dom";

const AllPlaces = (props) => {
  const allPlaces=[];

  return (
    <div>
      <ul>
        {allPlaces.map(p => (
          <li key={p.id}><Link to={`/place/${p.id}`}>{p.title}</Link></li>
        ))}
      </ul>
    </div>
  )
};

export default AllPlaces;
