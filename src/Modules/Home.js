import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Header from "./Header";

const useStyles = makeStyles((theme) => ({
    root: {

    }
}));

const Home = () => {
  const classes=useStyles();

  return (
    <React.Fragment>
    <Header />
    <div>
      <h1>Home page</h1>
    </div>
  </React.Fragment>
  )
};

export default Home;
