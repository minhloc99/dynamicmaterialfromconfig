import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import AllPlaces from "./AllPlaces";
// import Hanoi from "../components/Pages/Places/Hanoi";
// import Danang from "../components/Pages/Places/Danang";

const PlaceRouter = () => (
  <BrowserRouter>
    <Switch>
    <Route exact path="/place" component={AllPlaces} />
    {/* <Route path="/place/hanoi" component={Hanoi} />
    <Route path="/place/danang" component={Danang} /> */}
  </Switch></BrowserRouter>

);

export default PlaceRouter;
